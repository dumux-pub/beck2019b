# Beck2019b docker container

FROM phusion/baseimage:0.9.22
MAINTAINER martin.beck@iws.uni-stuttgart.de

# run Ubuntu update
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
    && apt-get upgrade -y -o Dpkg::Options::="--force-confold" \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# install the basic dependencies
RUN apt-get update \
    && apt-get install --no-install-recommends --yes \
    ca-certificates \
    vim \
    python3-dev \
    python3-pip \
    git \
    pkg-config \
    cmake \
    build-essential \
    gfortran \
    mpi-default-bin \
    mpi-default-dev \
    libsuitesparse-dev \
    libsuperlu-dev \
    libeigen3-dev \
    doxygen \
    wget \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# add the permission helper script to the my_init service
COPY setpermissions.sh /etc/my_init.d/setpermissions.sh

# create a dumux user
# add the welcome message (copied further down) output to bashrc
# make the set permission helper script executable
# add user to video group which enables graphics if desired
RUN useradd -m --home-dir /dumux dumux \
    && echo "cat /dumux/WELCOME" >> /dumux/.bashrc \
    && chmod +x /etc/my_init.d/setpermissions.sh \
    && usermod -a -G video dumux

# switch to the dumux user and set the working directory
USER dumux
WORKDIR /dumux

# create a shared volume communicating with the host
RUN mkdir /dumux/shared
VOLUME /dumux/shared

# This is the message printed on entry
COPY WELCOME /dumux/WELCOME

# set git user in case installation requires to apply patches
RUN git config --global user.name "Beck2019b"
RUN git config --global user.email "martin.beck@iws.uni-stuttgart.de"

# Install the dumux module and its dependencies
# This expects the install script to do everything from clone to configure
COPY installBeck2019b.sh /dumux/installBeck2019b.sh
RUN ./installBeck2019b.sh && rm -f /dumux/installBeck2019b.sh

# unset git user
RUN git config --global --unset user.name
RUN git config --global --unset user.email

# switch back to root
WORKDIR /dumux
USER root

# this sets the permissions right, see above
ENTRYPOINT ["/sbin/my_init","--quiet","--","/sbin/setuser","dumux","/bin/bash","-l","-c"]

# start interactive shell
CMD ["/bin/bash","-i"]
