// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
* \file
*
* \brief Adaption of the fully implicit scheme to the two-phase linear elasticity model.
*/

#ifndef DUMUX_ELASTIC2P_MODEL_HH
#define DUMUX_ELASTIC2P_MODEL_HH

#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dumux/common/eigenvalues.hh>
#include "properties.hh"

namespace Dumux {

namespace Properties {
NEW_PROP_TAG(InitialDisplacement); //!< The initial displacement function
NEW_PROP_TAG(InitialPressSat); //!< The initial pressure and saturation function
}

/*!
 * \ingroup ElTwoPBoxModel
 * \brief Adaption of the fully implicit scheme to the two-phase linear elasticity model.
 *
 * This model implements a two-phase flow of compressible immiscible fluids \f$\alpha \in \{ w, n \}\f$.
 * The deformation of the solid matrix is described with a quasi-stationary momentum balance equation.
 * The influence of the pore fluid is accounted for through the effective stress concept (Biot 1941 \cite Biot1941a).
 * The total stress acting on a rock is partially supported by the rock matrix and partially supported
 * by the pore fluid. The effective stress represents the share of the total stress which is supported
 * by the solid rock matrix and can be determined as a function of the strain according to Hooke's law.
 *
 * As an equation for the conservation of momentum within the fluid phases the standard multiphase Darcy's approach is used:
 \f[
 v_\alpha = - \frac{k_{r\alpha}}{\mu_\alpha} \textbf{K}
 \left(\textbf{grad}\, p_\alpha - \varrho_{\alpha} {\textbf g} \right)
 \f]
 *
 * Gravity can be enabled or disabled via the property system.
 * By inserting this into the continuity equation, one gets
\f[
 \frac{\partial \phi_{eff} \varrho_\alpha S_\alpha}{\partial t}
 - \text{div} \left\{ \varrho_\alpha \frac{k_{r\alpha}}{\mu_\alpha}
 \mathbf{K}_\text{eff} \left(\textbf{grad}\, p_\alpha - \varrho_{\alpha} \mathbf{g} \right)
 - \phi_{eff} \varrho_\alpha S_\alpha \frac{\partial \mathbf{u}}{\partial t}
 \right\} - q_\alpha = 0 \;,
 \f]
 *
 *
 * A quasi-stationary momentum balance equation is solved for the changes with respect to the initial conditions, note
 * that this implementation assumes the soil mechanics sign convention (i.e. compressive stresses are negative):
 \f[
 \text{div}\left( \boldsymbol{\Delta \sigma'}- \Delta p_{eff} \boldsymbol{I} \right) + \Delta \varrho_b {\textbf g} = 0 \;,
 \f]
 * with the effective stress:
 \f[
  \boldsymbol{\sigma'} = 2\,G\,\boldsymbol{\epsilon} + \lambda \,\text{tr} (\boldsymbol{\epsilon}) \, \mathbf{I}.
 \f]
 *
 * and the strain tensor \f$\boldsymbol{\epsilon}\f$ as a function of the solid displacement gradient \f$\textbf{grad} \mathbf{u}\f$:
 \f[
  \boldsymbol{\epsilon} = \frac{1}{2} \, (\textbf{grad} \mathbf{u} + \textbf{grad}^T \mathbf{u}).
 \f]
 *
 * Here, the rock mechanics sign convention is switch off which means compressive stresses are < 0 and tensile stresses are > 0.
 * The rock mechanics sign convention can be switched on for the vtk output via the property system.
 *
 * The effective porosity and the effective permeability are calculated as a function of the solid displacement:
 \f[
      \phi_{eff} = \frac{\phi_{init} + \text{div} \mathbf{u}}{1 + \text{div} \mathbf{u}}
 \f]
 \f[
      K_{eff} = K_{init} \text{exp}\left( 22.2(\phi_{eff}/\phi_{init} -1 )\right)
 \f]
 * The mass balance equations are discretized using a vertex-centered finite volume (box)
 * or cell-centered finite volume scheme as spatial and the implicit Euler method as time discretization.
 * The momentum balance equations are discretized using a standard Galerkin Finite Element method as
 * spatial discretization scheme.
 *
 *
 * The primary variables are the wetting phase pressure \f$p_w\f$, the nonwetting phase saturation \f$S_n\f$ and the solid
 * displacement vector \f$\mathbf{u}\f$ (changes in solid displacement with respect to initial conditions).
 */
template<class TypeTag>
class ElTwoPModel: public GET_PROP_TYPE(TypeTag, BaseModel)
{
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, BaseModel) ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        nPhaseIdx = Indices::nPhaseIdx,
        wPhaseIdx = Indices::wPhaseIdx
    };

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename Element::Geometry::JacobianInverseTransposed JacobianInverseTransposed;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;

    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, GridFunctionSpace) GridFunctionSpace;
    typedef Dune::PDELab::LocalFunctionSpace<GridFunctionSpace> LocalFunctionSpace;

public:

    /*!
     * \brief Write the current solution to a restart file.
     *
     * \param outStream The output stream of one vertex for the restart file
     * \param entity The Entity
     *
     * Due to the mixed discretization schemes which are combined via pdelab for this model
     * the solution vector has a different form than in the pure box models
     * it sorts the primary variables in the following way:
     * p_vertex0 S_vertex0 p_vertex1 S_vertex1 p_vertex2 ....p_vertexN S_vertexN
     * ux_vertex0 uy_vertex0 uz_vertex0 ux_vertex1 uy_vertex1 uz_vertex1 ...
     *
     * Therefore, the serializeEntity function has to be modified.
     */
    template <class Entity>
    void serializeEntity(std::ostream &outStream,
                         const Entity &entity)
    {
        // vertex index
        int dofIdxGlobal = this->dofMapper().index(entity);

        // write phase state
        if (!outStream.good()) {
            DUNE_THROW(Dune::IOError,
                       "Could not serialize vertex "
                       << dofIdxGlobal);
        }
        int numScv = this->gridView().size(dim);
        // get p and S entries for this vertex
        for (int eqIdx = 0; eqIdx < numEq-dim; ++eqIdx) {
            outStream << this->curSol().base()[dofIdxGlobal*(numEq-dim) + eqIdx][0]<<" ";
        }
        // get ux, uy, uz entries for this vertex
        for (int j = 0; j< dim; ++j)
            outStream << this->curSol().base()[numScv*(numEq-dim) + dofIdxGlobal*dim + j][0] <<" ";

        int vIdxGlobal = this->dofMapper().index(entity);
        if (!outStream.good())
            DUNE_THROW(Dune::IOError, "Could not serialize vertex " << vIdxGlobal);
    }

    /*!
     * \brief Reads the current solution for a vertex from a restart
     *        file.
     *
     * \param inStream The input stream of one vertex from the restart file
     * \param entity The Entity
     *
     * Due to the mixed discretization schemes which are combined via pdelab for this model
     * the solution vector has a different form than in the pure box models
     * it sorts the primary variables in the following way:
     * p_vertex0 S_vertex0 p_vertex1 S_vertex1 p_vertex2 ....p_vertexN S_vertexN
     * ux_vertex0 uy_vertex0 uz_vertex0 ux_vertex1 uy_vertex1 uz_vertex1 ...
     *
     * Therefore, the deserializeEntity function has to be modified.
     */
    template<class Entity>
    void deserializeEntity(std::istream &inStream, const Entity &entity)
    {
        int dofIdxGlobal = this->dofMapper().index(entity);

        if (!inStream.good()){
                DUNE_THROW(Dune::IOError,
                           "Could not deserialize vertex "
                           << dofIdxGlobal);
        }
        int numScv = this->gridView().size(dim);
        for (int eqIdx = 0; eqIdx < numEq-dim; ++eqIdx) {
        // read p and S entries for this vertex
        inStream >> this->curSol().base()[dofIdxGlobal*(numEq-dim) + eqIdx][0];}
        for (int j = 0; j< dim; ++j){
            // read ux, uy, uz entries for this vertex
            inStream >> this->curSol().base()[numScv*(numEq-dim) + dofIdxGlobal*dim + j][0];}
    }

    /*brief init function calls ParentType (ImplicitModel) to
     initialize GridView properly
     */
    void init(Problem &problem)
    {
        ParentType::init(problem);

        // initialise hasElementFailed
        unsigned numElements = this->gridView().size(0);
        hasElementFailed_.resize(numElements);
        faultAngle_.resize(numElements);
        stressSink_.resize(numElements);

        // initialise anyFailure
        anyFailure_ = false;
    }


    /*!
     * \brief \copybrief ImplicitModel::addOutputVtkFields
     *
     * Specialization for the ElOnePTwoCBoxModel, add one-phase two-component
     * properties, solid displacement, stresses, effective properties and the
     * process rank to the VTK writer.
     */
    template<class MultiWriter>
    void addOutputVtkFields(const SolutionVector &sol, MultiWriter &writer) {
        // check whether compressive stresses are defined to be positive
        // (rockMechanicsSignConvention_ == true) or negative
        rockMechanicsSignConvention_ =  GET_PARAM_FROM_GROUP(TypeTag, bool, Vtk, RockMechanicsSignConvention);

        typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;
        typedef Dune::BlockVector<Dune::FieldVector<double, dim> > VectorField;

        // create the required scalar and vector fields
        unsigned numVertices = this->gridView_().size(dim);
        unsigned numElements = this->gridView_().size(0);

        // create the required fields for vertex data
        ScalarField &pw = *writer.allocateManagedBuffer(numVertices);
        ScalarField &pn = *writer.allocateManagedBuffer(numVertices);
        ScalarField &pc = *writer.allocateManagedBuffer(numVertices);
        ScalarField &sw = *writer.allocateManagedBuffer(numVertices);
        ScalarField &sn = *writer.allocateManagedBuffer(numVertices);
        VectorField &displacement = *writer.template allocateManagedBuffer<Scalar, dim>(numVertices);
        VectorField &displacementRot = *writer.template allocateManagedBuffer<Scalar, dim>(numVertices);
        ScalarField &rhoW = *writer.allocateManagedBuffer(numVertices);
        ScalarField &rhoN = *writer.allocateManagedBuffer(numVertices);
        ScalarField &Te = *writer.allocateManagedBuffer(numVertices);

        // create the required fields for element data
        // effective stresses
        VectorField &deltaEffStressX = *writer.template allocateManagedBuffer<Scalar,
                dim>(numElements);
        VectorField &deltaEffStressY = *writer.template allocateManagedBuffer<Scalar,
                dim>(numElements);
        VectorField &deltaEffStressZ = *writer.template allocateManagedBuffer<Scalar,
                dim>(numElements);
        // total stresses
        VectorField &totalEffStressX = *writer.template allocateManagedBuffer<
                Scalar, dim>(numElements);
        VectorField &totalEffStressY = *writer.template allocateManagedBuffer<
                Scalar, dim>(numElements);
        VectorField &totalEffStressZ = *writer.template allocateManagedBuffer<
                Scalar, dim>(numElements);
        // total stresses rotated
        VectorField &totalEffStressRotatedX = *writer.template allocateManagedBuffer<
                Scalar, dim>(numElements);
        VectorField &totalEffStressRotatedY = *writer.template allocateManagedBuffer<
                Scalar, dim>(numElements);

        // initial stresses
        VectorField &initStressX = *writer.template allocateManagedBuffer<
                Scalar, dim>(numElements);
        VectorField &initStressY = *writer.template allocateManagedBuffer<
                Scalar, dim>(numElements);
        VectorField &initStressZ = *writer.template allocateManagedBuffer<
                Scalar, dim>(numElements);
        // principal stresses
        ScalarField &principalStress1 = *writer.allocateManagedBuffer(
                numElements);
        ScalarField &principalStress2 = *writer.allocateManagedBuffer(
                numElements);
        ScalarField &principalStress3 = *writer.allocateManagedBuffer(
                numElements);


        ScalarField &effKx = *writer.allocateManagedBuffer(numElements);
        ScalarField &effPorosity = *writer.allocateManagedBuffer(numElements);
        ScalarField &effectivePressure = *writer.allocateManagedBuffer(numElements);
        ScalarField &deltaEffPressure = *writer.allocateManagedBuffer(numElements);
        ScalarField &initialPressure = *writer.allocateManagedBuffer(numElements);
        ScalarField &pInit = *writer.allocateManagedBuffer(numElements);


        ScalarField &Pcrtens = *writer.allocateManagedBuffer(numElements);
        ScalarField &Pcrshe = *writer.allocateManagedBuffer(numElements);

        ScalarField &K = *writer.allocateManagedBuffer(numElements);
        ScalarField &stressSink = *writer.allocateManagedBuffer(numElements);
        ScalarField &faultAngle = *writer.allocateManagedBuffer(numElements);
        ScalarField &anglePrincipalStresses = *writer.allocateManagedBuffer(numElements);

        // set anyFailure to zero before evaluation
        anyFailure_ = false;

        // initialize cell stresses, cell-wise hydraulic parameters and cell pressure with zero


        for (unsigned int eIdx = 0; eIdx < numElements; ++eIdx) {
            deltaEffStressX[eIdx] = Scalar(0.0);
            if (dim >= 2)
                deltaEffStressY[eIdx] = Scalar(0.0);
            if (dim >= 3)
                deltaEffStressZ[eIdx] = Scalar(0.0);

            totalEffStressX[eIdx] = Scalar(0.0);
            if (dim >= 2)
                totalEffStressY[eIdx] = Scalar(0.0);
            if (dim >= 3)
                totalEffStressZ[eIdx] = Scalar(0.0);

            initStressX[eIdx] = Scalar(0.0);
            if (dim >= 2)
                initStressY[eIdx] = Scalar(0.0);
            if (dim >= 3)
                initStressZ[eIdx] = Scalar(0.0);

            principalStress1[eIdx] = Scalar(0.0);
            if (dim >= 2)
                principalStress2[eIdx] = Scalar(0.0);
            if (dim >= 3)
                principalStress3[eIdx] = Scalar(0.0);

            effPorosity[eIdx] = Scalar(0.0);
            effKx[eIdx] = Scalar(0.0);
            effectivePressure[eIdx] = Scalar(0.0);
            deltaEffPressure[eIdx] = Scalar(0.0);
            initialPressure[eIdx] = Scalar(0.0);
            pInit[eIdx] = Scalar(0.0);

            Pcrtens[eIdx] = Scalar(0.0);
            Pcrshe[eIdx] = Scalar(0.0);

            K[eIdx] = Scalar(0.0);
            stressSink[eIdx] = Scalar(0.0);
            faultAngle[eIdx] = Scalar(0.0);
            anglePrincipalStresses[eIdx] = Scalar(0.0);
        }

        // calculate principal stresses i.e. the eigenvalues of the total stress tensor
        Scalar a1, a2, a3;
        DimMatrix totalEffStress;
        DimVector eigenValues;

        ScalarField &rank = *writer.allocateManagedBuffer(numElements);


        FVElementGeometry fvGeometry;
        ElementVolumeVariables elemVolVars;

        const GridFunctionSpace& gridFunctionSpace = this->problem_().model().jacobianAssembler().gridFunctionSpace();
        const typename GridFunctionSpace::Ordering& ordering = gridFunctionSpace.ordering();
        // initialize start and end of element iterator
        // loop over all elements (cells)
        for (const auto& element : elements(this->gridView_(), Dune::Partitions::interior))
        {
            // get FE function spaces to calculate gradients (gradient data of momentum balance
            // equation is not stored in fluxvars since it is not evaluated at box integration point)
            // copy the values of the sol vector to the localFunctionSpace values of the current element
            LocalFunctionSpace localFunctionSpace(gridFunctionSpace);
            localFunctionSpace.bind(element);
            std::vector<Scalar> values(localFunctionSpace.size());
            for (typename LocalFunctionSpace::Traits::IndexContainer::size_type k=0; k<localFunctionSpace.size(); ++k)
            {
                const typename GridFunctionSpace::Ordering::Traits::DOFIndex& di = localFunctionSpace.dofIndex(k);
                typename GridFunctionSpace::Ordering::Traits::ContainerIndex ci;
                ordering.mapIndex(di.view(),ci);
                values[k] = sol[ci];
            }

            // local function space for solid displacement
            typedef typename LocalFunctionSpace::template Child<1>::Type DisplacementLFS;
            const DisplacementLFS& displacementLFS =localFunctionSpace.template child<1>();
            const unsigned int dispSize = displacementLFS.child(0).size();
            typedef typename DisplacementLFS::template Child<0>::Type ScalarDispLFS;
            // further types required for gradient calculations
            typedef typename ScalarDispLFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianType_V;
            typedef typename ScalarDispLFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeFieldType RF;

            unsigned int eIdx = this->problem_().model().elementMapper().index(element);
            rank[eIdx] = this->gridView_().comm().rank();

            fvGeometry.update(this->gridView_(), element);
            elemVolVars.update(this->problem_(), element, fvGeometry, false);

            bool MohrCoulombWorstCase = GET_RUNTIME_PARAM(TypeTag, Scalar,FailureParameters.MohrCoulombWorstCase);

            if (!MohrCoulombWorstCase)
            {
                faultAngle_[eIdx] = this->problem_().getAngleElement(eIdx);
            }

            // loop over all local vertices of the cell
            int numScv = element.subEntities(dim);

            for (int scvIdx = 0; scvIdx < numScv; ++scvIdx)
            {
                unsigned int vIdxGlobal = this->dofMapper().subIndex(element, scvIdx, dim);

                Te[vIdxGlobal] = elemVolVars[scvIdx].temperature();
                pw[vIdxGlobal] = elemVolVars[scvIdx].pressure(wPhaseIdx);
                pn[vIdxGlobal] = elemVolVars[scvIdx].pressure(nPhaseIdx);
                pc[vIdxGlobal] = elemVolVars[scvIdx].capillaryPressure();
                sw[vIdxGlobal] = elemVolVars[scvIdx].saturation(wPhaseIdx);
                sn[vIdxGlobal] = elemVolVars[scvIdx].saturation(nPhaseIdx);
                rhoW[vIdxGlobal] = elemVolVars[scvIdx].density(wPhaseIdx);
                rhoN[vIdxGlobal] = elemVolVars[scvIdx].density(nPhaseIdx);
                // the following lines are correct for rock mechanics sign convention
                // but lead to a very counter-intuitive output therefore, they are commented.
                // in case of rock mechanics sign convention solid displacement is
                // defined to be negative if it points in positive coordinate direction
//                if(rockMechanicsSignConvention_){
//                    DimVector tmpDispl;
//                    tmpDispl = Scalar(0);
//                    tmpDispl -= elemVolVars[scvIdx].displacement();
//                    displacement[vIdxGlobal] = tmpDispl;
//                    }
//
//                else
                    displacement[vIdxGlobal] = elemVolVars[scvIdx].displacement();
                    displacementRot[vIdxGlobal] = this->calculateRotatedDisplacement(displacement[vIdxGlobal], faultAngle_[eIdx]);

                double Keff;
                double exponent;
                exponent = 22.2    * (elemVolVars[scvIdx].effPorosity
                            / elemVolVars[scvIdx].porosity() - 1);
                Keff =    this->problem_().spatialParams().intrinsicPermeability(    element, fvGeometry, scvIdx)[0][0];
                using std::exp;
                Keff *= exp(exponent);
                effKx[eIdx] += Keff/ numScv;
                effectivePressure[eIdx] += (pn[vIdxGlobal] * sn[vIdxGlobal]
                                            + pw[vIdxGlobal] * sw[vIdxGlobal])
                                            / numScv;
                effPorosity[eIdx] +=elemVolVars[scvIdx].effPorosity / numScv;
            };

            const auto geometry = element.geometry();

            const GlobalPosition& cellCenter = geometry.center();
            const GlobalPosition& cellCenterLocal = geometry.local(cellCenter);

            deltaEffPressure[eIdx] = effectivePressure[eIdx] + this->problem_().pInit(cellCenter, cellCenterLocal, element);
            initialPressure[eIdx] = -1.0 * this->problem().pInit(cellCenter, cellCenterLocal, element);
            pInit[eIdx] = this->problem_().pInit(cellCenter, cellCenterLocal, element);

            // determin changes in effective stress from current solution
            // evaluate gradient of displacement shape functions
            std::vector<JacobianType_V> vRefShapeGradient(dispSize);
            displacementLFS.child(0).finiteElement().localBasis().evaluateJacobian(cellCenterLocal, vRefShapeGradient);

            // get jacobian to transform the gradient to physical element
            const JacobianInverseTransposed jacInvT = geometry.jacobianInverseTransposed(cellCenterLocal);
            std::vector < Dune::FieldVector<RF, dim> > vShapeGradient(dispSize);
            for (size_t i = 0; i < dispSize; i++) {
                vShapeGradient[i] = 0.0;
                jacInvT.umv(vRefShapeGradient[i][0], vShapeGradient[i]);
            }
            // calculate gradient of current displacement
            typedef Dune::FieldMatrix<RF, dim, dim> DimMatrix;
            DimMatrix uGradient(0.0);
            for (int coordDir = 0; coordDir < dim; ++coordDir) {
                const ScalarDispLFS & scalarDispLFS = displacementLFS.child(coordDir);

                for (size_t i = 0; i < scalarDispLFS.size(); i++)
                    uGradient[coordDir].axpy(values[scalarDispLFS.localIndex(i)],vShapeGradient[i]);
            }

            const Dune::FieldVector<Scalar, 2> lameParams =    this->problem_().spatialParams().lameParams(element,fvGeometry, 0);
            const Scalar lambda = lameParams[0];
            const Scalar mu = lameParams[1];

            K[eIdx] = lambda + 2.0/3.0*mu;
            stressSink[eIdx] = stressSink_[eIdx];

            // calculate strain tensor
            Dune::FieldMatrix<RF, dim, dim> epsilon;
            for (int i = 0; i < dim; ++i)
                for (int j = 0; j < dim; ++j)
                    epsilon[i][j] = 0.5 * (uGradient[i][j] + uGradient[j][i]);

            RF traceEpsilon = 0;
            for (int i = 0; i < dim; ++i)
                traceEpsilon += epsilon[i][i];

            // calculate effective stress tensor
            Dune::FieldMatrix<RF, dim, dim> sigma(0.0);
            for (int i = 0; i < dim; ++i) {
                sigma[i][i] = lambda * traceEpsilon;
                for (int j = 0; j < dim; ++j)
                    sigma[i][j] += 2.0 * mu * epsilon[i][j];
            }

            if(hasElementFailed_[eIdx])
                if (eIdx == 1)
                    sigma = this->calculateReducedStress(eIdx, sigma, true);
                else
                    sigma = this->calculateReducedStress(eIdx, sigma, false);
            // in case of rock mechanics sign convention compressive stresses
            // are defined to be positive
            if(rockMechanicsSignConvention_){
                deltaEffStressX[eIdx] -= sigma[0];
                if (dim >= 2) {
                    deltaEffStressY[eIdx] -= sigma[1];
                }
                if (dim >= 3) {
                    deltaEffStressZ[eIdx] -= sigma[2];
                }
            }
            else{
                deltaEffStressX[eIdx] = sigma[0];
                if (dim >= 2) {
                    deltaEffStressY[eIdx] = sigma[1];
                }
                if (dim >= 3) {
                    deltaEffStressZ[eIdx] = sigma[2];
                }
            }

            // retrieve prescribed initial stresses from problem file
            DimVector tmpInitStress = this->problem_().initialStress(cellCenter);
            if(rockMechanicsSignConvention_){
                initStressX[eIdx][0] = tmpInitStress[0];
                if (dim >= 2) {
                    initStressY[eIdx][1] = tmpInitStress[1];
                    }
                if (dim >= 3) {
                    initStressZ[eIdx][2] = tmpInitStress[2];
                }
            }
            else{
                initStressX[eIdx][0] -= tmpInitStress[0];
                if (dim >= 2) {
                    initStressY[eIdx][1] -= tmpInitStress[1];
                    }
                if (dim >= 3) {
                    initStressZ[eIdx][2] -= tmpInitStress[2];
                }
            }

            // calculate total stresses
            // in case of rock mechanics sign convention compressive stresses
            // are defined to be positive and total stress is calculated by adding the pore pressure
            if(rockMechanicsSignConvention_){
                totalEffStressX[eIdx][0] = initStressX[eIdx][0] + deltaEffStressX[eIdx][0]    - initialPressure[eIdx];
                if (dim >= 2) {
                    totalEffStressX[eIdx][1] = initStressX[eIdx][1] + deltaEffStressX[eIdx][1];
                    totalEffStressY[eIdx][0] = initStressY[eIdx][0] + deltaEffStressY[eIdx][0];
                    totalEffStressY[eIdx][1] = initStressY[eIdx][1] + deltaEffStressY[eIdx][1]    - initialPressure[eIdx];
                }
                if (dim >= 3) {
                    totalEffStressX[eIdx][2] = initStressX[eIdx][2] + deltaEffStressX[eIdx][2];
                    totalEffStressY[eIdx][2] = initStressY[eIdx][2] + deltaEffStressY[eIdx][2];
                    totalEffStressZ[eIdx][0] = initStressZ[eIdx][0] + deltaEffStressZ[eIdx][0];
                    totalEffStressZ[eIdx][1] = initStressZ[eIdx][1] + deltaEffStressZ[eIdx][1];
                    totalEffStressZ[eIdx][2] = initStressZ[eIdx][2] + deltaEffStressZ[eIdx][2]    - initialPressure[eIdx];
                }
            }
            else{
                totalEffStressX[eIdx][0] = initStressX[eIdx][0] + deltaEffStressX[eIdx][0]    + initialPressure[eIdx];
                if (dim >= 2) {
                    totalEffStressX[eIdx][1] = initStressX[eIdx][1] + deltaEffStressX[eIdx][1];
                    totalEffStressY[eIdx][0] = initStressY[eIdx][0] + deltaEffStressY[eIdx][0];
                    totalEffStressY[eIdx][1] = initStressY[eIdx][1] + deltaEffStressY[eIdx][1]    + initialPressure[eIdx];
                }
                if (dim >= 3) {
                    totalEffStressX[eIdx][2] = initStressX[eIdx][2] + deltaEffStressX[eIdx][2];
                    totalEffStressY[eIdx][2] = initStressY[eIdx][2] + deltaEffStressY[eIdx][2];
                    totalEffStressZ[eIdx][0] = initStressZ[eIdx][0] + deltaEffStressZ[eIdx][0];
                    totalEffStressZ[eIdx][1] = initStressZ[eIdx][1] + deltaEffStressZ[eIdx][1];
                    totalEffStressZ[eIdx][2] = initStressZ[eIdx][2] + deltaEffStressZ[eIdx][2]    + initialPressure[eIdx];
                }
            }

            Dune::FieldMatrix<RF, dim, dim> deltaEffStress(0.0);
            deltaEffStress[0][0] = deltaEffStressX[eIdx][0];
            deltaEffStress[0][1] = deltaEffStressX[eIdx][1];
            deltaEffStress[1][0] = deltaEffStressY[eIdx][0];
            deltaEffStress[1][1] = deltaEffStressY[eIdx][1];

//             Dune::FieldMatrix<RF, dim, dim> totalEffStress(0.0);
            totalEffStress[0][0] = totalEffStressX[eIdx][0];
            totalEffStress[0][1] = totalEffStressX[eIdx][1];
            totalEffStress[1][0] = totalEffStressY[eIdx][0];
            totalEffStress[1][1] = totalEffStressY[eIdx][1];

            // rotate total stress tensor
            Dune::FieldMatrix<RF, dim, dim> totalEffStressRotated(0.0);
            totalEffStressRotated = this->calculateRotatedStress(totalEffStress, faultAngle_[eIdx]);

            // rotate delta stress tensor
            Dune::FieldMatrix<RF, dim, dim> deltaEffStressRotated(0.0);
            deltaEffStressRotated = this->calculateRotatedStress(deltaEffStress, faultAngle_[eIdx]);

//             if( (eIdx == 1) /*|| (eIdx == 1025)*/)
// //             if(this->problem().getHasElementFailed(eIdx))
//             {
//                 std::cout << "element " << eIdx << " is inclined by " << faultAngle_[eIdx] << std::endl;
//
//                 std::cout << "deltaEffStress:" << std::endl;
//                 std::cout << deltaEffStress[0][0] << " " << deltaEffStress[0][1] << std::endl;
//                 std::cout << deltaEffStress[1][0] << " " << deltaEffStress[1][1] << std::endl;
//                 std::cout << "" << std::endl;
//
//                 std::cout << "deltaEffStressRotated:" << std::endl;
//                 std::cout << deltaEffStressRotated[0][0] << " " << deltaEffStressRotated[0][1] << std::endl;
//                 std::cout << deltaEffStressRotated[1][0] << " " << deltaEffStressRotated[1][1] << std::endl;
//                 std::cout << "" << std::endl;
//
//                 std::cout << "totalEffStress:" << std::endl;
//                 std::cout << totalEffStress[0][0] << " " << totalEffStress[0][1] << std::endl;
//                 std::cout << totalEffStress[1][0] << " " << totalEffStress[1][1] << std::endl;
//                 std::cout << "" << std::endl;
//
//                 std::cout << "totalEffStressRotated:" << std::endl;
//                 std::cout << totalEffStressRotated[0][0] << " " << totalEffStressRotated[0][1] << std::endl;
//                 std::cout << totalEffStressRotated[1][0] << " " << totalEffStressRotated[1][1] << std::endl;
//                 std::cout << "" << std::endl;
//             }

            if (dim == 2) {
                totalEffStressRotatedX[eIdx][0] = totalEffStressRotated[0][0];
                totalEffStressRotatedX[eIdx][1] = totalEffStressRotated[0][1];
                totalEffStressRotatedY[eIdx][0] = totalEffStressRotated[1][0];
                totalEffStressRotatedY[eIdx][1] = totalEffStressRotated[1][1];
            }


            eigenValues = Scalar(0);
//             totalEffStress = Scalar(0);
//
//             totalEffStress[0] = totalEffStressX[eIdx];
//             if (dim >= 2)
//                 totalEffStress[1] = totalEffStressY[eIdx];
//             if (dim >= 3)
//                 totalEffStress[2] = totalEffStressZ[eIdx];

            calculateEigenValues<dim>(eigenValues, totalEffStress);


            for (int i = 0; i < dim; i++)
                {
                using std::isnan;
                if (isnan(eigenValues[i]))
                    eigenValues[i] = 0.0;
                }

            // sort principal stresses: principalStress1 >= principalStress2 >= principalStress3
            if (dim == 2) {
                a1 = eigenValues[0];
                a2 = eigenValues[1];

                if (a1 >= a2) {
                    principalStress1[eIdx] = a1;
                    principalStress2[eIdx] = a2;
                } else {
                    principalStress1[eIdx] = a2;
                    principalStress2[eIdx] = a1;
                }
            }

            if (dim == 3) {
                a1 = eigenValues[0];
                a2 = eigenValues[1];
                a3 = eigenValues[2];

                if (a1 >= a2) {
                    if (a1 >= a3) {
                        principalStress1[eIdx] = a1;
                        if (a2 >= a3) {
                            principalStress2[eIdx] = a2;
                            principalStress3[eIdx] = a3;
                        }
                        else //a3 > a2
                        {
                            principalStress2[eIdx] = a3;
                            principalStress3[eIdx] = a2;
                        }
                    }
                    else // a3 > a1
                    {
                        principalStress1[eIdx] = a3;
                        principalStress2[eIdx] = a1;
                        principalStress3[eIdx] = a2;
                    }
                } else // a2>a1
                {
                    if (a2 >= a3) {
                        principalStress1[eIdx] = a2;
                        if (a1 >= a3) {
                            principalStress2[eIdx] = a1;
                            principalStress3[eIdx] = a3;
                        }
                        else //a3>a1
                        {
                            principalStress2[eIdx] = a3;
                            principalStress3[eIdx] = a1;
                        }
                    }
                    else //a3>a2
                    {
                        principalStress1[eIdx] = a3;
                        principalStress2[eIdx] = a2;
                        principalStress3[eIdx] = a1;
                    }
                }
            }
            Scalar taum  = 0.0;
            Scalar sigmam = 0.0;
            Scalar Peff = effectivePressure[eIdx];

            const Dune::FieldVector<Scalar,2> failureCurveParams = this->problem_().spatialParams().failureCurveParams(element, fvGeometry, 0);
            Scalar frictionAngle = failureCurveParams[0];
            Scalar cohesion = failureCurveParams[1];

            if (dim == 2)
            {
                taum = (principalStress1[eIdx] - principalStress2[eIdx]) / 2;
                sigmam = (principalStress1[eIdx] + principalStress2[eIdx]) / 2;
            }
            if (dim == 3)
            {
                taum = (principalStress1[eIdx] - principalStress3[eIdx]) / 2;
                sigmam = (principalStress1[eIdx] + principalStress3[eIdx]) / 2;
            }

            if (MohrCoulombWorstCase == true)
            {
                if(!hasElementFailed_[eIdx])
                {

                    Scalar Psc = -fabs(taum) / sin(frictionAngle) + cohesion * cos(frictionAngle) / sin(frictionAngle) + sigmam;
                    Pcrshe[eIdx] = Peff - Psc;

                    // angle between xy and principalStresses:
                    // tan (2*angle) = 2 tau_xy / (sigma_x - sigma_y)
                    if(std::abs(totalEffStressX[eIdx][1]) > eps_)
                        anglePrincipalStresses[eIdx] = atan(2.0 * totalEffStressX[eIdx][1] / (totalEffStressX[eIdx][0] - totalEffStressY[eIdx][1]) ) / 2.0 / M_PI * 180.0;

                    // angle between principal stresses and sigma_fault, Worst Case:
                    // (90 + frictionAngle)/2
                    Scalar angleB = (90 + frictionAngle/ M_PI * 180.0)/2;


                    if(anglePrincipalStresses[eIdx] > eps_)
                        faultAngle_[eIdx] = angleB-anglePrincipalStresses[eIdx];
                    else
                        faultAngle_[eIdx] = -angleB-anglePrincipalStresses[eIdx];
                }
                else{
                    Scalar sigmaFault = 0.0;
                    Scalar tauFault = 0.0;
                    if (dim == 2)
                    {
                        sigmaFault = 0.5 * (totalEffStressX[eIdx][0] + totalEffStressY[eIdx][1]) + 0.5 * (totalEffStressX[eIdx][0] - totalEffStressY[eIdx][1])*cos(2.0*faultAngle_[eIdx] * M_PI / 180.0) + totalEffStressX[eIdx][1]*sin(2.0*faultAngle_[eIdx] * M_PI / 180.0);
                        tauFault   =                                                       - 0.5 * (totalEffStressX[eIdx][0] - totalEffStressY[eIdx][1])*sin(2.0*faultAngle_[eIdx] * M_PI / 180.0) + totalEffStressX[eIdx][1]*cos(2.0*faultAngle_[eIdx] * M_PI / 180.0);
                        Scalar sigmaFailureCurve = (std::abs(tauFault) - cohesion) / tan(frictionAngle);

                        Pcrshe[eIdx] =  sigmaFailureCurve - sigmaFault;
                        if ( Pcrshe[eIdx] > eps_ )
                        {
                            std::cout << "sigmaFault [ " << eIdx << "] is " << sigmaFault << std::endl;
                            std::cout << "tauFault [ " << eIdx << "] is " << tauFault << std::endl;
                            std::cout << "sigmaFailureCurve [ " << eIdx << "] is " << sigmaFailureCurve << std::endl;
                        }
                    }
                    if (dim == 3)
                    {
                        std::cout << "WARNING: Inclined elements only implemented in 2D" << std::endl;
                    }
                }



            }
            else
            {
                Scalar sigmaFault = 0.0;
                Scalar tauFault = 0.0;
                if (dim == 2)
                {
                    sigmaFault = 0.5 * (totalEffStressX[eIdx][0] + totalEffStressY[eIdx][1]) + 0.5 * (totalEffStressX[eIdx][0] - totalEffStressY[eIdx][1])*cos(2.0*faultAngle_[eIdx] * M_PI / 180.0) + totalEffStressX[eIdx][1]*sin(2.0*faultAngle_[eIdx] * M_PI / 180.0);
                    tauFault   =                                                       - 0.5 * (totalEffStressX[eIdx][0] - totalEffStressY[eIdx][1])*sin(2.0*faultAngle_[eIdx] * M_PI / 180.0) + totalEffStressX[eIdx][1]*cos(2.0*faultAngle_[eIdx] * M_PI / 180.0);
                    Scalar sigmaFailureCurve = (std::abs(tauFault) - cohesion) / tan(frictionAngle);

                    Pcrshe[eIdx] =  sigmaFailureCurve - sigmaFault;
                    if ( Pcrshe[eIdx] > eps_ )
                    {
                        std::cout << "sigmaFault [ " << eIdx << "] is " << sigmaFault << std::endl;
                        std::cout << "tauFault [ " << eIdx << "] is " << tauFault << std::endl;
                        std::cout << "sigmaFailureCurve [ " << eIdx << "] is " << sigmaFailureCurve << std::endl;
                    }
                }
                if (dim == 3)
                {
                    std::cout << "WARNING: Inclined elements only implemented in 2D" << std::endl;
                }

            }

            faultAngle[eIdx] = faultAngle_[eIdx];

            // principal Stress is in fact the effective principal stress
            if (dim == 2)
                Pcrtens[eIdx] = principalStress2[eIdx];
            if (dim == 3)
                Pcrtens[eIdx] = principalStress3[eIdx];

            bool initializationRun = this->problem_().initializationRun();
            if (initializationRun == false)
            {
                    //bool isElementFailed = false;
                if ( Pcrshe[eIdx] > eps_ )
                {
                    anyFailure_ = true;

                    hasElementFailed_[eIdx] = true;
                    std::cout << "Pcrshe of Element [ " << eIdx << "] is " << Pcrshe[eIdx] << "\n";

                    Scalar deltaSigma = GET_RUNTIME_PARAM(TypeTag, Scalar,FailureParameters.DeltaSigma);

                    // totalEffStressRotated[0][1] has the opposite sign of sigma
                    if (totalEffStressRotated[0][1] < eps_)
                    {
                        stressSink_[eIdx] -= deltaSigma;
                    }
                    else
                    {
                        stressSink_[eIdx] += deltaSigma;
                    }

                }
            }

        }

        writer.attachVertexData(Te, "T");
        writer.attachVertexData(pw, "pW");
        writer.attachVertexData(pn, "pN");
        writer.attachVertexData(pc, "pC");
        writer.attachVertexData(sw, "SW");
        writer.attachVertexData(sn, "SN");
        writer.attachVertexData(rhoW, "rhoW");
        writer.attachVertexData(rhoN, "rhoN");
        writer.attachVertexData(displacement, "u", dim);
        writer.attachVertexData(displacementRot, "uRot", dim);

        writer.attachCellData(deltaEffStressX, "effectiveStressChangesX", dim);
        if (dim >= 2)
            writer.attachCellData(deltaEffStressY, "effectiveStressChangesY",    dim);
        if (dim >= 3)
            writer.attachCellData(deltaEffStressZ, "effectiveStressChangesZ",    dim);

        writer.attachCellData(principalStress1, "principalstress1");
        if (dim >= 2)
            writer.attachCellData(principalStress2, "principalstress2");
        if (dim >= 3)
            writer.attachCellData(principalStress3, "principalstress3");

        writer.attachCellData(totalEffStressX, "totalstressesX", dim);
        if (dim >= 2)
            writer.attachCellData(totalEffStressY, "totalstressesY", dim);
        if (dim >= 3)
            writer.attachCellData(totalEffStressZ, "totalstressesZ", dim);

        if (dim == 2)
        {
            writer.attachCellData(totalEffStressRotatedX, "totalstressesRotatedX", dim);
            writer.attachCellData(totalEffStressRotatedY, "totalstressesRotatedY", dim);
        }

        writer.attachCellData(initStressX, "initialstressesX", dim);
        if (dim >= 2)
            writer.attachCellData(initStressY, "initialstressesY", dim);
        if (dim >= 3)
            writer.attachCellData(initStressZ, "initialstressesZ", dim);

        writer.attachCellData(deltaEffPressure, "deltapEff");
        writer.attachCellData(pInit, "pInit");
        writer.attachCellData(effectivePressure, "effectivePressure");
        writer.attachCellData(Pcrtens, "Pcr_tensile");
        writer.attachCellData(Pcrshe, "Pcr_shear");
        writer.attachCellData(effKx, "effectiveKxx");
        writer.attachCellData(effPorosity, "effectivePorosity");

        writer.attachCellData(K, "K");
        writer.attachCellData(stressSink, "stressSink");
        writer.attachCellData(faultAngle, "faultAngle");
        writer.attachCellData(anglePrincipalStresses, "anglePrincipalStresses");
    }

    bool hasElementFailed(const int eIdx) const
    {
        return hasElementFailed_[eIdx];
    }

    bool anyFailure() const
    {
        return anyFailure_;
    }

    DimVector calculateRotatedDisplacement(const DimVector originalDisplacement, Scalar angle) const
    {
        DimVector rotatedDisplacement(0.0);

        // rotate stress vector in direction of element
        Scalar s = sin(angle / 180 * M_PI);
        Scalar c = cos(angle / 180 * M_PI);

        rotatedDisplacement[0] = c * originalDisplacement[0] - s *  originalDisplacement[1];
        rotatedDisplacement[1] = s * originalDisplacement[0] + c *  originalDisplacement[1];

        return  rotatedDisplacement;
    }

    Dune::FieldMatrix<Scalar,dim,dim> calculateRotatedStress(const Dune::FieldMatrix<Scalar,dim,dim> originalStress, Scalar angle) const
    {
        Dune::FieldMatrix<Scalar,dim,dim> rotatedStress(0.0);

        // rotate stress vector in direction of element
        Scalar s = sin(2* angle / 180 * M_PI);
        Scalar c = cos(2* angle / 180 * M_PI);

        Scalar sigmam   = (originalStress[0][0] + originalStress[1][1])/2;
        Scalar sigmadev = (originalStress[0][0] - originalStress[1][1])/2;

        rotatedStress[0][0] = sigmam + sigmadev * c + originalStress[0][1] * s;
        rotatedStress[1][1] = sigmam - sigmadev * c - originalStress[0][1] * s;
        rotatedStress[0][1] = - sigmadev * s + originalStress[0][1] * c;
        rotatedStress[1][0] = rotatedStress[0][1];

        return  rotatedStress;
    }

    Dune::FieldMatrix<Scalar,dim,dim> calculateReducedStress(int eIdx, Dune::FieldMatrix<Scalar,dim,dim> originalStress, bool output)
    {
        Dune::FieldMatrix<Scalar,dim,dim> reducedStress(0.0);

        if(output)
        {
            std::cout << "originalStress:" << std::endl;
            std::cout << originalStress[0][0] << " " << originalStress[0][1] << std::endl;
            std::cout << originalStress[1][0] << " " << originalStress[1][1] << std::endl;
            std::cout << "" << std::endl;
        }

        if (dim == 2)
        {
            Dune::FieldMatrix<Scalar,dim,dim> rotatedStress(0.0);
            bool MohrCoulombWorstCase = GET_RUNTIME_PARAM(TypeTag, Scalar,FailureParameters.MohrCoulombWorstCase);
            Scalar angle = 0.0;

            if (MohrCoulombWorstCase)
                angle = faultAngle_[eIdx];
            else
                angle = this->problem_().getAngleElement(eIdx);

            if(output)
            {
                std::cout << "element " << eIdx << " is inclined by " << angle << std::endl;
            }

            rotatedStress = calculateRotatedStress(originalStress, angle);

            if(output)
            {
                std::cout << "rotatedStress:" << std::endl;
                std::cout << rotatedStress[0][0] << " " << rotatedStress[0][1] << std::endl;
                std::cout << rotatedStress[1][0] << " " << rotatedStress[1][1] << std::endl;
                std::cout << "" << std::endl;
            }

            rotatedStress[0][1] += stressSink_[eIdx];
            rotatedStress[1][0] += stressSink_[eIdx];



            if(output)
            {
                std::cout << "rotatedStressReduced:" << std::endl;
                std::cout << rotatedStress[0][0] << " " << rotatedStress[0][1] << std::endl;
                std::cout << rotatedStress[1][0] << " " << rotatedStress[1][1] << std::endl;
                std::cout << "" << std::endl;
            }

            reducedStress = calculateRotatedStress(rotatedStress, -angle);

        }
        else
        {
            std::cout << "WARNING: Inclined elements only implemented in 2D" << std::endl;
        }
        if(output)
        {
            std::cout << "reducedStress:" << std::endl;
            std::cout << reducedStress[0][0] << " " << reducedStress[0][1] << std::endl;
            std::cout << reducedStress[1][0] << " " << reducedStress[1][1] << std::endl;
            std::cout << "" << std::endl;
        }

        return reducedStress;
    }

    /*!
     * \brief Applies the initial solution for all vertices of the grid.
     */
    void applyInitialSolution_() {
        typedef typename GET_PROP_TYPE(TypeTag, InitialPressSat) InitialPressSat;
        InitialPressSat initialPressSat(this->problem_().gridView());
        std::cout << "el2pmodel calls: initialPressSat" << std::endl;
        initialPressSat.setPressure(this->problem_().pInit());

        typedef typename GET_PROP_TYPE(TypeTag, InitialDisplacement) InitialDisplacement;
        InitialDisplacement initialDisplacement(this->problem_().gridView());

        typedef Dune::PDELab::CompositeGridFunction<InitialPressSat,
                InitialDisplacement> InitialSolution;
        InitialSolution initialSolution(initialPressSat, initialDisplacement);

        int numDofs = this->jacobianAssembler().gridFunctionSpace().size();
        //this->curSol().resize(numDofs);
        //this->prevSol().resize(numDofs);
        std::cout << "numDofs = " << numDofs << std::endl;

        Dune::PDELab::interpolate(initialSolution,
                this->jacobianAssembler().gridFunctionSpace(), this->curSol());
        Dune::PDELab::interpolate(initialSolution,
                this->jacobianAssembler().gridFunctionSpace(), this->prevSol());
    }

    const Problem& problem() const {
        return this->problem_();
    }

private:
    bool anyFailure_;
    std::vector<bool> hasElementFailed_;
    std::vector<Scalar> faultAngle_;

    std::vector<Scalar> stressSink_;

    bool rockMechanicsSignConvention_;
    Scalar eps_ = 1e-6;

};
}
#include "propertydefaults.hh"
#endif
