#!/bin/sh

### Get DUNE modules
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.4
cd ..
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.4
cd ..
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.4
cd ..
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.4
cd ..
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.4
cd ..

# download dune-PDELab
git clone https://gitlab.dune-project.org/pdelab/dune-pdelab.git
cd dune-pdelab
git checkout releases/2.0
cd ..

wget -q https://git.iws.uni-stuttgart.de/dumux-pub/beck2019a/raw/master/ResolveCompilerPaths.cmake

# download dune-typetree
git clone https://gitlab.dune-project.org/staging/dune-typetree
cd dune-typetree
git checkout releases/2.3
cd ..

git clone https://gitlab.dune-project.org/extensions/dune-alugrid
cd dune-alugrid
git checkout releases/2.4
cd ..

# fix this file in dune-pdelab
mv ResolveCompilerPaths.cmake dune-pdelab/cmake/modules/

# Get Dumux
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout releases/2.12
cd ..

# Get the beck2019b module
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Beck2019b.git


# # run dunecontrol
./dune-common/bin/dunecontrol --opts=./dumux/optim.opts all

