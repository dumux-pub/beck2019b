Installation
============

The easiest way to install this module and its dependencies is to create a new
directory and to execute the file
[installBeck2019b.sh](https://git.iws.uni-stuttgart.de/dumux-pub/beck2019b/raw/master/installBeck2019b.sh)
in this directory.

```bash
mkdir -p Beck2019b && cd Beck2019b
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/beck2019b/raw/master/installBeck2019b.sh
chmod u+x installBeck2019b.sh
./installBeck2019b.sh
```
For a detailed information on installation have a look at the DuMuX installation guide or use the DuMuX handbook, chapter 2.

Installation with Docker 
========================

Create a new folder in your favourite location and download the container startup script to that folder.
```bash
mkdir Beck2019b
cd Beck2019b
wget https://git.iws.uni-stuttgart.de/dumux-pub/beck2019b/-/raw/master/docker_beck2019b.sh
```

Open the Docker Container by running
```bash
bash docker_beck2019b.sh open
```

Folder structure
============

The module beck2019b contains within the folder *dumux/geomechanics/el2p* the source code for conceptual model.
The folder *test/geomechanics/el2p* contains the problem specific source code for the
lab-scale (triaxial_el2p) and field-scale test case (test_el2p).

Run Simulations
===============

Running simulations is possible via (exemplary for the field-scale test case)
```bash
cd Beck2019b/build-cmake/test/geomechanics/el2p
make test_el2p
./test_el2p
```
In the docker container, it is not nessesary to compile the source codes as all the binaries are shipped with the container.
